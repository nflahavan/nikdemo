//
//  ViewController.swift
//  NikDemo
//
//  Created by NFlahavan on 8/27/18.
//  Copyright © 2018 NFlahavan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    
    var flipCount = 0 {
        didSet {
            flipsLabel.text = "flips: \(flipCount)"
        }
    }
    
    @IBOutlet weak var flipsLabel: UILabel!
    @IBOutlet var cardButtons: [UIButton]!
    
    @IBAction func touchCard(_ sender: UIButton) {
        flipCount += 1
        let cardNumber = cardButtons.index(of: sender)!
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    @IBAction func touchNewGame(_ sender: UIButton) {
        game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
        usableEmojis = emojiChoices
        flipCount = 0
        updateViewFromModel()
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControlState.normal)
                button.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControlState.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
    }
    
    let emojiChoices = ["👻","🎃","👽","💀","👹", "🤡", "🤠", "🤖"]
    var usableEmojis = ["👻","🎃","👽","💀","👹", "🤡", "🤠", "🤖"]
    
    var emoji = [Int:String]()
    
    func emoji (for card: Card) -> String{
        if emoji[card.identifier] == nil, usableEmojis.count > 0 {
            let randomIndex = Int(arc4random_uniform(UInt32(usableEmojis.count)))
            emoji[card.identifier] = usableEmojis.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }
}

